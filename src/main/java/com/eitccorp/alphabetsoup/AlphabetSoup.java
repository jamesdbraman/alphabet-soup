package com.eitccorp.alphabetsoup;

import java.io.*;
import java.util.*;

/**
 * The AlphabetSoup class provides functionalities to:
 * 1. Load a word search puzzle from a file.
 * 2. Search for and print the coordinates of the words in the puzzle.
 *
 * <p>Design Decisions:
 * - The Trie data structure achieves O(n) prefix lookup matching
 * -     as opposed to O(n^2) nested for loops checking for matches in all directions.
 * - The 2nd part of the input file (puzzle) is captured as a 2D char array.
 * - A nested loop is used to iterate through the puzzle and search for words in all 8 possible directions.
 *       I believe this could also be done with streams, mapping each character evaluation to a thread for parallel evaluation, 
 *       but seemed like overkill for this assignment.
 *
 * @author Jim Braman
 */
public class AlphabetSoup {

    char[][] puzzle; 
    Trie trie = new Trie(); // Trie data structure to efficiently manage the words to be searched
    List<String> wordsList = new ArrayList<>(); // List of words to be found

    /**
     * Entry point
     *
     * @param args Command-line arguments. Expects one argument: the path to the input file.
     */
    public static void main(String[] args) throws IOException {
        // Ensure there's exactly one command-line argument
        if (args.length != 1) {
            System.out.println("Usage: java AlphabetSoup <filepath>");
            return;
        }

        AlphabetSoup soup = new AlphabetSoup();
        soup.loadFile(args[0]);
        soup.searchWords();
    }

    /**
     * Load the word search puzzle and words from the given file.
     *
     * @param filePath Path to the input file.
     * @throws IOException If there's an error reading the file.
     */
    void loadFile(String filePath) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(filePath))) {
            String[] dimensions = br.readLine().split("x");
            int rows = Integer.parseInt(dimensions[0]);
            int cols = Integer.parseInt(dimensions[1]);

            // Initialize the puzzle based on dimensions from the input
            puzzle = new char[rows][cols];

            // Populate the puzzle with characters from the file
            for (int i = 0; i < rows; i++) {
                String[] chars = br.readLine().split(" ");
                for (int j = 0; j < cols; j++) {
                    puzzle[i][j] = chars[j].charAt(0);
                }
            }

            // Load words into the Trie and the list
            String line;
            while ((line = br.readLine()) != null) {
                trie.insert(line);
                wordsList.add(line);
            }
        }
    }

    /**
     * Search for each word in the wordList on the puzzle, print its starting and ending coordinates.
     */
    void searchWords() {
        for (String word : wordsList) {
            boolean found = false;
            outerLoop:
            for (int i = 0; i < puzzle.length; i++) {
                for (int j = 0; j < puzzle[i].length; j++) {
                    // Check for the word starting at coordinates (i, j)
                    int[] endCoords = checkWord(i, j, word);
                    if (endCoords != null) {
                        System.out.println(word + " " + i + ":" + j + " " + endCoords[0] + ":" + endCoords[1]);
                        found = true;
                        break outerLoop;
                    }
                }
            }
            if (!found) {
                System.out.println(word + " not found");
            }
        }
    }

    /**
     * Check if a word exists starting from a given position (x, y) on the puzzle.
     * The word is checked in all 8 possible directions.
     *
     * @param x    Starting x-coordinate
     * @param y    Starting y-coordinate
     * @param word Word to search for
     * @return An int array representing the ending coordinates of the word if found, null otherwise.
     */
    int[] checkWord(int x, int y, String word) {
        for (int[] direction : Trie.directions) {
            int i = x, j = y, k = 0;
            // Move in the direction while matching characters
            while (k < word.length() &&
                    i >= 0 && i < puzzle.length &&
                    j >= 0 && j < puzzle[i].length &&
                    puzzle[i][j] == word.charAt(k)) {
                i += direction[0];
                j += direction[1];
                k++;
            }
            // If the word is fully matched, return its ending coordinates
            if (k == word.length()) return new int[]{i - direction[0], j - direction[1]};
        }
        return null;
    }

    /**
     * TrieNode class represents a single node in the Trie.
     * Each node contains a map of child nodes and a flag indicating the end of a word.
     */
    static class TrieNode {
        HashMap<Character, TrieNode> children = new HashMap<>();
        boolean isEndOfWord = false;
    }

    /**
     * Trie class provides functionalities to insert words and check their existence.
     * The Trie uses TrieNode as its building block.
     */
    static class Trie {
        TrieNode root = new TrieNode();

        // Eight possible directions: vertical, horizontal, and diagonal.
        static final int[][] directions = {
            {-1, -1}, {-1, 0}, {-1, 1}, {0, -1}, {0, 1}, {1, -1}, {1, 0}, {1, 1}
        };

        /**
         * Inserts a word into the Trie.
         *
         * @param word The word to be inserted.
         */
        void insert(String word) {
            TrieNode node = root;
            for (char c : word.toCharArray()) {
                // Compute if absent is used to insert a character only if it doesn't exist
                node = node.children.computeIfAbsent(c, k -> new TrieNode());
            }
            node.isEndOfWord = true;
        }
    }
}
