package com.eitccorp.alphabetsoup;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.io.TempDir;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class AlphabetSoupTest {
AlphabetSoup alphabetSoup;

    @BeforeEach
    void setUp() {
        alphabetSoup = new AlphabetSoup();
    }

    @Test
    void testLoadFile(@TempDir Path tempDir) throws IOException {
        Path inputFile = tempDir.resolve("input.txt");
        Files.write(inputFile, "3x3\nA B C\nD E F\nG H I\nABC\nDEF\nGHI".getBytes());

        alphabetSoup.loadFile(inputFile.toString());

        // Check if puzzle is properly loaded
        assertEquals('A', alphabetSoup.puzzle[0][0]);
        assertEquals('I', alphabetSoup.puzzle[2][2]);

        // Check if wordsList is properly loaded
        assertTrue(alphabetSoup.wordsList.contains("ABC"));
        assertTrue(alphabetSoup.wordsList.contains("DEF"));
        assertTrue(alphabetSoup.wordsList.contains("GHI"));
    }

    @Test
    void testSearchWords(@TempDir Path tempDir) throws IOException {
        Path inputFile = tempDir.resolve("input.txt");
        Files.write(inputFile, "3x3\nA B C\nD E F\nG H I\nABC\nDEF\nGHI".getBytes());
        
        alphabetSoup.loadFile(inputFile.toString());

        // Capture the output stream stdout
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));

        alphabetSoup.searchWords();

        // Convert the captured output stream stdout to String
        String output = new String(bos.toByteArray());

        String expectedOutput = "ABC 0:0 0:2\nDEF 1:0 1:2\nGHI 2:0 2:2\n";
        assertEquals(expectedOutput, output);
    }

    @Test
    void testCheckWord() {
        alphabetSoup.puzzle = new char[][]{
                {'A', 'B', 'C'},
                {'D', 'E', 'F'},
                {'G', 'H', 'I'}
        };

        // Testing for word ABC starting from 0,0
        int[] result = alphabetSoup.checkWord(0, 0, "ABC");
        assertNotNull(result);
        assertEquals(0, result[0]);
        assertEquals(2, result[1]);

        // Testing for a word not on the puzzle
        assertNull(alphabetSoup.checkWord(0, 0, "XYZ"));
    }

    @Test
    void testTrieInsertAndSearch() {
        AlphabetSoup.Trie trie = new AlphabetSoup.Trie();
        trie.insert("HELLO");

        assertTrue(trie.root.children.containsKey('H'));
        assertTrue(trie.root.children.get('H').children.containsKey('E'));
    }

    @ParameterizedTest
    @MethodSource("testData")
    void testMain(String inputFile, String expectedOutput) {
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        System.setOut(new PrintStream(bos));

        // Prepare the arguments
        String[] args = {inputFile};

        // Invoke main method
        try {
            AlphabetSoup.main(args);
        } catch (IOException e) {
            e.printStackTrace();
        }

        String output = new String(bos.toByteArray());

        // Assert the output against the expected output
        assertEquals(expectedOutput, output);
    }

    // This method provides the test data for the parameterized test
    private static Stream<Arguments> testData() {
        return Stream.of(
            // Sample Test Provided in Project README
            Arguments.of("src/test/resources/inputSample.txt", "HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1\n"),
            // Test for direction changes / words that are not found
            Arguments.of("src/test/resources/inputDirectionChange.txt", "HELLO 0:0 4:4\nGOOD not found\nBYE 1:3 1:1\n"),
            // Test for two words that share a Trie root path
            Arguments.of("src/test/resources/inputSharedRoot.txt", "HELLO 0:0 4:4\nGOOD 4:0 4:3\nBYE 1:3 1:1\nHAS 0:0 0:2\nHASH 0:0 0:3\n"),
            // Test for large files (I generated this file locally with a quick script)
            Arguments.of("src/test/resources/inputLarger.txt", "KVJXUCZ 17:11 23:17\nPAMJOT 3:3 3:8\nNISR 8:1 8:4\nHUGWPWNIA 10:13 18:13\nZGKUVDLQOT 12:24 21:24\nZYVJR 19:23 23:23\nQKQGTAP 23:9 23:15\nKHXR 19:18 22:18\nVVNTOCSOTY 13:14 13:23\nVSI 11:12 13:12\n")
        );
    }
}
